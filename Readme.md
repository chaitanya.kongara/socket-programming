<pre>
Firstly we need to compile and execute server.c(present in server directory) followed by compiling and executing client.c(present in client directory).

compiling server.c:
    `cd server` (changing to server directory)
    `gcc server.c -o server` (compiling server.c and naming the executable as server).
    `./server` (executing the executable).
compiling client.c:
    `cd client` (changing to client directory)
    `gcc client.c -o client` (compiling client.c and naming the executable as client).
    `./client` (executing the executable).
    
 Once after the connection between server and client gets established, use get command to download files from the server side.
 
 client> get file1 file2 file3
 
 Checks if the requested files are present on the server side, if present files will get downloaded and progress of the file transfer will also be printed onto the terminal, else error message will be displayed.
 <pre />

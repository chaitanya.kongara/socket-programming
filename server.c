#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include<signal.h>
#define PORT 10000
void sighandler(){
    printf("There is no client on the other end!\n");
}
int main(int argc, char const *argv[])
{
    signal(SIGPIPE,sighandler);
    int server_fd, new_socket, valread;
    struct sockaddr_in address;  
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    // Creating socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)  // creates socket, SOCK_STREAM is for TCP. SOCK_DGRAM for UDP
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // This is to lose the pesky "Address already in use" error message
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                                                  &opt, sizeof(opt))) // SOL_SOCKET is the socket layer itself
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;  // Address family. For IPv6, it's AF_INET6. 29 others exist like AF_UNIX etc. 
    address.sin_addr.s_addr = INADDR_ANY;  // Accept connections from any IP address - listens from all interfaces.
    address.sin_port = htons( PORT );    // Server port to open. Htons converts to Big Endian - Left to Right. RTL is Little Endian

    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr *)&address,
                                 sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Port bind is done. You want to wait for incoming connections and handle them in some way.
    // The process is two step: first you listen(), then you accept()
    if (listen(server_fd, 3) < 0) // 3 is the maximum size of queue - connections you haven't accepted
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    while(1){
    // returns a brand new socket file descriptor to use for this single accepted connection. Once done, use send and recv
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                       (socklen_t*)&addrlen))<0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    char *input;
    char **split;
    char *size;
    char *buf;
    while(1){
        input = (char *)malloc(1024);
        split=(char **)malloc(1024*sizeof(char *));
        for(int i=0;i<1024;i++) split[i]=(char *)malloc(1024*sizeof(char));
        int chai=read(new_socket , input , 1024);
        if(chai==0) break;
        if(input[0]=='e') break;
        int k=0,j=0;
        for(int i=4;i<strlen(input);i++){
            if(input[i]==' '){
                split[k][j]='\0';
                j=0;
                k++;
                continue;
            }
            split[k][j++]=input[i];
        }
        split[k][j]='\0';
        int fd[k+1],no_file[k+1];
        for(int i=0;i<=k;i++){
            no_file[i]=0;
            fd[i]=open(split[i],O_RDONLY,0777);
            if(fd[i]<0) no_file[i]=1;
        }
        char err_msg[1024]="unable to download the following files:";
        int f=39;
        for(int i=0;i<=k;i++){
            if(no_file[i]){
                err_msg[f++]=' ';
                for(int m=0;split[i][m]!='\0';m++){
                    err_msg[f++]=split[i][m];
                }
            }
        }
        err_msg[f]='\0';
        if(f>39) printf("%s\n",err_msg);
        send(new_socket, err_msg , 1024 , 0);
        //printf("%d\n",k+1);
        for(int i=0;i<=k;i++){
            if(no_file[i]==0){
                size=(char *)malloc(1024);
                send(new_socket , split[i] , 1024 , 0);
                long long int siz=lseek(fd[i],0,SEEK_END);
                sprintf(size,"%lld",siz);
                send(new_socket,size,1024,0);
                lseek(fd[i],0,SEEK_SET);
                printf("%s:name %lld:siz\n",split[i],siz);
                while(siz>=10000){
                    buf=(char *)malloc(10000);
                    read(fd[i],buf,10000);
                    int cnt=0;
                    for(int i=0;buf[i]!='\0';i++) cnt++;
                    //printf("%d:siz %d:cnt\n",siz,cnt);
                    //printf("%s\n",buf);
                    send(new_socket,buf,10000,0);
                    siz-=10000;
                    free(buf);
                }
                if(siz>0){
                    buf=(char *)malloc(10000);
                    int ret=read(fd[i],buf,10000);
                    int cnt=0;
                    buf[siz]='\0';
                    for(int i=0;buf[i]!='\0';i++) cnt++;
                    int sen=send(new_socket,buf,10000,0);
                    //printf("%d:siz %d:cnt %d:ret %d:sen\n",siz,cnt,ret,sen);
                    //printf("%s\n",buf);
                    free(buf);
                    siz=0;
                }
                free(size);
                close(fd[i]);
            }
        }
        for(int i=0;i<1024;i++) free(split[i]);
        free(split);
        free(input);
    }
    close(new_socket);
    }
    return 0;
}

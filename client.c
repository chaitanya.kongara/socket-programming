// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#define PORT 10000

void beautify(char *input){
    char temp[1024];
    int k=0,i=0;
    while(input[i]==' ') i++;
    while(input[i]!='\0'){
        if(input[i]==' '){
            temp[k++]=' ';
            while(input[i]==' ') i++;
        }
        else{
            temp[k++]=input[i++];
        }
    }
    for(int i=0;i<k;i++){
        input[i]=temp[i];
    }
    input[k]='\0';
    return ;
}
int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr)); // to make sure the struct is empty. Essentially sets sin_zero as 0
                                                // which is meant to be, and rest is defined below

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Converts an IP address in numbers-and-dots notation into either a 
    // struct in_addr or a struct in6_addr depending on whether you specify AF_INET or AF_INET6.
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)  // connect to the server address
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    else printf("Connected to server \n");
    char *input;
    char *err_msg;
    char *fname;
    char *size;
    char *buf;
    int exit=0;
    while (exit==0)
    {
        input=(char *)malloc(1024);
        err_msg=(char *)malloc(1024);
        printf("client> ");
        fgets(input,1024,stdin);
        input[strlen(input)-1]='\0';
        beautify(input);
        int car=0,f=0;
        for(int i=0;i<strlen(input);i++) if(input[i]==' ')car++;
        if(strlen(input)==4&&input[0]=='e'&&input[1]=='x'&&input[2]=='i'&&input[3]=='t') {printf("closing connection with server!\n");send(sock , input , 1024 , 0);exit=1;}
        else if(strlen(input)>4&&input[0]=='g'&&input[1]=='e'&&input[2]=='t'&&input[3]==' '){
            send(sock , input , 1024 , 0);
            read(sock , err_msg , 1024);
            for(int i=0;i<strlen(err_msg);i++) if(err_msg[i]==' ') f++;
            f-=5;
            if(strlen(err_msg)>39){
                for(int i=0;err_msg[i]!='\0';i++) printf("%c",err_msg[i]);
                printf("\n");
            }
            int loop=car-f;
            //printf("%d %d\n",car,f);
            while(loop--){
                fname=(char *)malloc(1024);
                read(sock,fname,1024);
                printf("%s:\n",fname);
                int fd=open(fname,O_RDWR | O_TRUNC | O_CREAT,0777);
                lseek(fd,0,SEEK_SET);
                size=(char *)malloc(1024);
                read(sock , size, 1024);
                //printf("%s:size\n",size);
                long long int siz=atoll(size);
                long long int max=siz;
                //printf("%d:siz\n",siz);
                //free(buf);
                printf("Downloading..... - %.2f%c",100.00-100*((siz+0.00)/(max+0.00)),'%');
                while(siz>=10000){
                    buf=(char *)malloc(10000);
                    read(sock,buf,10000);
                    int cnt=0;
                    for(int i=0;buf[i]!='\0';i++) cnt++;
                    //printf("%d:siz %d:cnt\n",siz,cnt);
                    //printf("%s\n",buf);
                    siz-=10000;
                    write(fd,buf,10000);
                    printf("\rDownloading..... - %.2f%c",100.00-100*((siz+0.00)/(max+0.00)),'%');
                    free(buf);
                }
                if(siz>0){
                    buf=(char *)malloc(10000);
                    int ret=read(sock,buf,10000);
                    int cnt=0;
                    buf[siz]='\0';
                    for(int i=0;buf[i]!='\0';i++) cnt++;
                    //printf("%d:siz %d:cnt %d:ret\n",siz,cnt,ret);
                    //printf("%s\n",buf);
                    write(fd,buf,siz);
                    free(buf);
                    siz=0;
                }
                printf("\rDownloading completed..... - 100%c\n",'%');
                free(size);
                free(fname);
                close(fd);
            }
            free(err_msg);
        }
        else{
            printf("Available commands : `exit` and `get <files>`\n");
        }
        free(input);
    }
    close(sock);
    // send(sock , hello , strlen(hello) , 0 );  // send the message.
    // printf("Hello message sent\n");
    // valread = read( sock , buffer, 1024);  // receive message back from server, into the buffer
    // printf("%s\n",buffer);
    return 0;
}
